package com.example.alexa.project;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Benboubaker on 25/02/16.
 */
public class Sudoku {
    private int grille_[][];
    private final int TAILLE_GRILLE = 9;
    private final int NB_CASE = 9*9;
    private int nbSolution;
    private ArrayList<Integer> caseVide;

    public Sudoku(int nbVal){
        caseVide = new ArrayList<Integer>();
        grille_  = new int[TAILLE_GRILLE+1][TAILLE_GRILLE+1];

        for(int i = 0 ; i <= TAILLE_GRILLE; ++i)
            for(int j = 0 ; j <= TAILLE_GRILLE; ++j)
                grille_[i][j] = 0; 				// mettre toute les case a zero

        for(int i = 0 ; i < 9; ++i) {   				// pré-remplir certaine case au hazard
            int rdX 	= (int) (Math.random() * TAILLE_GRILLE);
            int rdY 	= (int) (Math.random() * TAILLE_GRILLE);
            int rdVal 	= (int) (Math.random() * TAILLE_GRILLE)+1;

            if(estPossible(rdX, rdY, rdVal) && grille_[rdX][rdY] != rdVal)
                grille_[rdX][rdY] = rdVal;
        }
        genereGrille(0);  // permet d'avoir une grille resolvable
        mettreCertaineCaseàVide(nbVal);
        nbSolution = 0;
    }

    public Sudoku(String s[][]){
        caseVide = new ArrayList<Integer>();
        grille_  = new int[TAILLE_GRILLE+1][TAILLE_GRILLE+1];

        for(int i = 0 ; i <= TAILLE_GRILLE; ++i)
            for(int j = 0 ; j <= TAILLE_GRILLE; ++j)
                grille_[i][j] = Integer.parseInt(s[i][j]); 				// mettre toute les case a zero
    }

    /*
    Sudoku(int t[][]){
        caseVide = new ArrayList<Integer>();
        grille_  = new int[TAILLE_GRILLE+1][TAILLE_GRILLE+1];
        nbSolution = 0;

        for(int i = 0; i < NB_CASE ; ++i){
            grille_[i/9][i%9] = t[i/9][i%9];
            if(grille_[i/9][i%9] == 0)
                caseVide.add(i);
        }
    } */

    public int getNbSolution() {
        return nbSolution;
    }

    private boolean absentSurLigne(int val, int ligne){
        for (int j = 0; j < TAILLE_GRILLE; j++)
            if (grille_[ligne][j] == val)
                return false;
        return true;
    }

    private boolean absentSurColonne(int val, int colonne){
        for (int i=0; i < TAILLE_GRILLE; ++i)
            if (grille_[i][colonne] == val)
                return false;
        return true;
    }

    private boolean absentSurBloc(int val, int i, int j){
        int x = i-(i%3), y = j-(j%3);
        for (i=x; i < x+3; i++)
            for (j=y; j < y+3; j++)
                if (grille_[i][j] == val)
                    return false;
        return true;
    }

    public int[][] getGrille_() {
        return grille_;
    }

    public void setGrille_(int[][] grille_) {
        this.grille_ = grille_;
    }

    /**
     * Vérifie que la valeur, et les coordonnées passer en paramatre soient incérable dans la grille
     * @param x : coord x
     * @param y : coord y
     * @param i : valeur a placer
     * @return vrai si c'est possible, faux si non
     */
    private boolean estPossible(int x, int y, int i){
        if( absentSurLigne(i, x) && absentSurColonne(i,y) && absentSurBloc(i, x, y) )
            return true;
        return false;
    }

    /**
     * Arbre est une fonction récursive qui va rechercher toute les solutions possible
     * @param pos: si pos est égale a la taille de la liste des cases vide on a trouver une solution
     */
    public void arbre(int pos){
        int[] b = new int[9];

        if (pos == caseVide.size()){
            nbSolution += 1;
            Log.i("sudoku","resolu");
            Log.i("sudoku",this.toString());
            return;
           // System.out.println(this);
           // System.out.println(this.getNbSolution());

        }
        else {
            int x = caseVide.get(pos)/9;
            int y = caseVide.get(pos)%9;
            int k = 0;

            for(int nb=1; nb <= TAILLE_GRILLE;++nb)
                if (estPossible(x,y,nb))   // verif de 1 à 9 si il est possible de mettre cette valeur
                    b[k++]=nb;			// si c'est possible mettre cette valeur dans B et incrémenté k

            for(int i=0;i<k;i++){     	// for sur la taille de B et mettre la valeur dans B dans X et Y qu'on cherche
                grille_[x][y] = b[i];
                arbre(pos+1);
            }
            grille_[x][y]=0; // s'execute dans le cas ou k est null cad pas de chiffre possible
        }
    }

    /**
     * Générer une grille resolvable
     * @param pos: la premiere case de la grille
     * @return une grille resolvable
     */
    private boolean genereGrille (int pos){
        if (pos == NB_CASE)  return true;

        if (grille_[pos/9][pos%9] != 0)
            return genereGrille(pos+1);

        for (int k=1; k <= 9; k++){
            if ( estPossible(pos/9, pos%9, k)){
                grille_[pos/9][pos%9] = k;
                if (genereGrille(pos+1))
                    return true;
            }
        }
        grille_[pos/9][pos%9] = 0;
        return false;
    }

    /**
     * mettre certaine case a zero
     * @param nbVal: nombre de valeur qu'il faut mettre zero cad case devient vide
     */
    private void mettreCertaineCaseàVide(int nbVal){
        int rd = 0;
        for(int i = 0; i < (NB_CASE-nbVal) ; ++i){
            rd = (int) (Math.random() * 81);
            if( grille_[rd/9][rd%9] != 0 && !caseVide.contains(rd))
                grille_[rd/9][rd%9] = 0;

            else	i--;
        }

        int cmp = 0;
        for(int i = 0 ; i < TAILLE_GRILLE; ++i)
            for(int j = 0 ; j < TAILLE_GRILLE; ++j, cmp++)
                if(grille_[i][j] == 0)
                    caseVide.add(cmp);
    }

    public String toString(){
        String s = "";
        for(int i = 0 ; i < TAILLE_GRILLE; ++i){
            for(int j = 0 ; j < TAILLE_GRILLE; ++j)
                s += (grille_[i][j] == 0) ?  ". " : grille_[i][j] + " ";
            s += '\n';
        }
        return s;
    }
}
